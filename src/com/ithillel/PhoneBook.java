package com.ithillel;

import java.util.Arrays;
import java.util.Objects;

public class PhoneBook {

    private static Record[] EMPTY = new Record[0];
    private Record[] records = EMPTY;

    public PhoneBook(Record[] records) {
        this.records = records;
    }

    @Override
    public String toString() {
        return "PhoneBook{" +
                "records=" + Arrays.toString(records) +
                '}';
    }

    public void add(Record record) {
        Record[] addedrec = new Record[records.length + 1];
        for (int i = 0; i < addedrec.length - 1; i++) {
            addedrec[i] = records[i];
            addedrec[i + 1] = record;
        }
        System.out.println(
                "PhoneBook:\n" + Arrays.toString(addedrec)
        );
    }

    public Record get(String name) {

        for (int i = 0; i < records.length; i++) {
            if (name.equals(records[i].getName())) {
                System.out.println("match found: \n" + records[i].toString());
                return records[i];
            }
        }
        System.out.println("null");
        return null;
    }


    public Record[] getAll(String name) {
        Record[] foundRecords = new Record[0];

        for (int i = 0; i < records.length; i++) {
            if (records[i].getName().equals(name)) {
                foundRecords = addRecord(records[i], foundRecords);
            }
            System.out.println("matches found: \n" + Arrays.toString(foundRecords));
            return foundRecords;
        }

        return EMPTY;
    }

    public Record[] addRecord(Record newRecord, Record[] target) {
        Record[] addedRecord = new Record[target.length + 1];
        for (int i = 0; i < target.length; i++) {
            addedRecord[i] = target[i];
        }
        addedRecord[addedRecord.length - 1] = newRecord;
        System.out.println(Arrays.toString(addedRecord));
        return addedRecord;
    }
}

